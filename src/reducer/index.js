import { ADD_COUNTER, REMOVE_COUNTER } from "../action";

export const reducer = (state = { value: 0 }, action) => {
  switch (action.type) {
    case ADD_COUNTER:
      return {
        ...state,
        value: state.value + 1,
      };
    case REMOVE_COUNTER:
      return {
        ...state,
        value: state.value - 1,
      };
    default:
      return state;
  }
};
