import { useDispatch, useSelector } from "react-redux";
import "./App.css";

function App() {
  const dispatch = useDispatch();

  const { value } = useSelector((state) => state);
  const add = () => {
    dispatch({ type: "ADD_COUNTER_ASYNC" });
  };

  const remove = () => {
    dispatch({ type: "REMOVE_COUNTER_ASYNC" });
  };

  return (
    <div>
      <button type={"button"} onClick={add}>
        ajouter
      </button>
      <button type={"button"} onClick={remove}>
        supprimer
      </button>
      nombre : {value}
    </div>
  );
}

export default App;
