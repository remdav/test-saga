export const ADD_COUNTER = {
  type: "ADD_COUNTER",
};
export const REMOVE_COUNTER = {
  type: "REMOVE_COUNTER",
};
export const ADD_COUNTER_ASYNC = {
  type: "ADD_COUNTER_ASYNC",
};
