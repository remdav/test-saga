import { all, put, takeEvery } from "redux-saga/effects";
import { ADD_COUNTER, REMOVE_COUNTER } from "../action";

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

export function* incrementAsync() {
  yield delay(3000);
  yield put({ type: ADD_COUNTER });
}

export function* incrementRemoveAsync() {
  yield delay(3000);
  yield put({ type: REMOVE_COUNTER });
}

export function* asyncAddMethod() {
  yield takeEvery("ADD_COUNTER_ASYNC", incrementAsync);
}

export function* asyncRemoveMethod() {
  yield takeEvery("REMOVE_COUNTER_ASYNC", incrementRemoveAsync);
}

export default function* rootSaga() {
  yield all([asyncAddMethod(), asyncRemoveMethod()]);
}
